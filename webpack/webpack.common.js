const VueLoaderPlugin = require('vue-loader/lib/plugin')
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// Is the current build a development build
const IS_DEV = process.env.NODE_ENV === 'dev';

const dirNode = 'node_modules';
const dirApp = path.join(__dirname, '../src/js');
// const dirAssets = path.join(__dirname, 'assets');

const appHtmlTitle = 'Webpack Boilerplate!!!';

/**
 * Webpack Configuration
 */
module.exports = {
  entry: {
    // vendor: [
    //   'lodash'
    // ],
    bundle: path.join(dirApp, 'index')
  },
  resolve: {
    modules: [dirNode, dirApp]
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.DefinePlugin({
      IS_DEV: IS_DEV
    }),

    new HtmlWebpackPlugin({
      template: path.join(__dirname, '../src/index.pug'),
      title: appHtmlTitle
    })
  ],
  module: {
    rules: [
      // Vue
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      // BABEL
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /(node_modules)/
      },
      // PUG
      {
        test: /\.pug$/,
        loader: 'pug-loader',
        exclude: /(node_modules)/
      },

      // STYLES
      {
        test: /\.s?css$/,
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          'postcss-loader'
        ]
      }
    ]
  }
};
