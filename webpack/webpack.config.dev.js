const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const webpackConfig = require('./webpack.common');

module.exports = merge(webpackConfig, {
  devtool: 'source-map',

  output: {
    pathinfo: true,
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: '[name].js'
  },

  devServer: {
    host: '0.0.0.0',
    port: 1234,
    hot: true
  },
  plugins: [new webpack.HotModuleReplacementPlugin()]
});
