module.exports = {
  "presets": [
    "vue",
    // "@babel/preset-flow",
    [
      "@babel/preset-env",
      {
        "useBuiltIns": "usage",
        "targets": {
          "browsers": [
            "> 0.2% in JP"
          ]
        },
        "modules": false,
        "loose": true
      }
    ]
  ],
  // "env": {
  //   "test": {
  //     "plugins": [
  //       "@babel/plugin-transform-modules-commonjs"
  //     ]
  //   }
  // },
  "ignore": [
    "node_modules"
  ]
  // "plugins": [
  //   ["@babel/plugin-proposal-decorators", {
  //     "legacy": true
  //   }],
  //   [
  //     "@babel/plugin-transform-runtime",
  //     {
  //       "helpers": false,
  //       "regenerator": true
  //     }
  //   ],
  //   ["@babel/plugin-proposal-pipeline-operator", { "proposal": "minimal" }],
  //   "lodash",
  //   "react-hot-loader/babel",
  //   "@babel/plugin-transform-flow-strip-types",
  //   "@babel/plugin-proposal-class-properties",
  //   "@babel/plugin-syntax-dynamic-import",
  //   [
  //     "module-resolver",
  //     {
  //       "root": ["./src/js"],
  //       "alias": {
  //         "@common": "./src/js/common"
  //       }
  //     }
  //   ]
  // ]
}
