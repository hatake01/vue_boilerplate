# webpackの環境構築
## 目標
vue.jsの環境をゼロから構築する
### 前提
- nodo や npm はインストール済み
- .gitignore[ここから](https://github.com/github/gitignore/blob/master/Node.gitignore)拝借
## webpackのインストール
### webpack本体
```console
$ npm i -D webpack webpack-cli webpack-dev-server
```
### webpackのプラグイン
```console
$ npm i -D html-webpack-plugin webpack-merge uglifyjs-webpack-plugin
```
コマンドライン環境変数をmac,win問わず使えるようにする
```console
$ npm i -D cross-env
```
## 各種サイト構築に必要なモジュールやローダーのインストール
[loaderって何かについてはこちらを参考](https://qiita.com/terrierscript/items/0574ab1ef358fecb55b9#loader%E3%81%A8plugin%E3%81%AE%E9%81%95%E3%81%84)
### Vue.js
```console
$ npm i vue
$ npm i -D vue-loader
```
### CSSまわり(postcss使用)
```console
$ npm i -D css-loader postcss-loader 
```
### Babel
```console
$ npm i -D babel-loader @babel/cli @babel/core @babel/preset-env @babel/polyfill
```
### pug
```console
$ npm i -D pug pug-plain-loader
```
